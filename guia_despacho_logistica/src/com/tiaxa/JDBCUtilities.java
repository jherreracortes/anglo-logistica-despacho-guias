package com.tiaxa;

import javax.sql.DataSource;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;



public class JDBCUtilities {


    public static <T> ArrayList<T> getResultSetGeneric(DataSource ds, RowMapper<T> rowmapper ,String sql,  Object... queryArgs) {
        //log.debug(printArgs(sql,queryArgs));
        PreparedStatement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        int index = 0;
        ArrayList<T> resultList = new ArrayList<T>();

        try {
            connection  = ds.getConnection();
            stmt = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY,  ResultSet.CONCUR_READ_ONLY);
            //stmt.setFetchSize(Integer.MIN_VALUE);
            setPreparedStatementArgs(stmt, queryArgs);
            rs = stmt.executeQuery();
            int row = 0;
            while (rs.next()) {
                row++;
                T t = rowmapper.mapRow(rs, row);
                resultList.add(t);
            }
        } catch (SQLException  e) {
            System.err.println("ERRORr: getResultSetGeneric() SQLException ");
	    e.printStackTrace(System.err);
            return null;
        }
        catch (Exception e) {
            System.err.println("ERROR: getResultSetGeneric() Exception ");
	    e.printStackTrace(System.err);
            return null;
        }

        finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
            	System.err.println("ERROR: getResultSetGeneric() Silent SQLException ");
		e.printStackTrace(System.err);
            }
        }
        return resultList;
    }
    
    
    
   protected static String printArgs(String sqlQuery, Object[] args)
   { 
	return String.format("query \"%s\" - params %s",sqlQuery, Arrays.deepToString(args));
   }

    private static void setPreparedStatementArgs(PreparedStatement preparedStatement, Object queryArgs[])
            throws SQLException {
        int index = 0;
	if ( queryArgs != null )
        for (Object query_arg : queryArgs) {
            index++;
            if (query_arg == null)
                preparedStatement.setNull(index, Types.VARCHAR);
            else if (query_arg instanceof Integer)
                preparedStatement.setInt(index, (Integer) query_arg);
            else if (query_arg instanceof String)
                preparedStatement.setString(index, (String) query_arg);
            else if (query_arg instanceof Float)
                preparedStatement.setFloat(index, (Float) query_arg);
            else if (query_arg instanceof Double)
                preparedStatement.setDouble(index, (Double) query_arg);
            else if (query_arg instanceof byte[])
                preparedStatement.setBytes(index, (byte[]) query_arg);
            else if (query_arg instanceof Byte)
                preparedStatement.setByte(index, (Byte) query_arg);
            else if (query_arg instanceof Date)
                preparedStatement.setDate(index, (Date) query_arg);
            else if (query_arg instanceof Timestamp)
                preparedStatement.setTimestamp(index, (Timestamp) query_arg);
            else if (query_arg instanceof Time)
                preparedStatement.setTime(index, (Time) query_arg);
            else if (query_arg instanceof Long)
                preparedStatement.setLong(index, (Long) query_arg);
            else if (query_arg instanceof Short)
                preparedStatement.setShort(index, (Short) query_arg);
            else
                preparedStatement.setObject(index, query_arg);
        }
    }

    public static int executeUpdate(DataSource pool, String sql, Object... queryArgs) 
    {
        return executeUpdate(pool,sql,false,queryArgs);
    }

    public static int executeUpdate(DataSource pool,
                                     String sql, boolean getGeneratedKey, Object... queryArgs) {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int index = 0;
        //log.debug(printArgs(sql,queryArgs));
        try {
            connection = pool.getConnection();
            if (getGeneratedKey)
                stmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            else
                stmt = connection.prepareStatement(sql);
            setPreparedStatementArgs(stmt, queryArgs);
            index = stmt.executeUpdate();
            if (getGeneratedKey) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    index = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            System.err.println("ERROR: executeUpdate() SQLException ");
	    e.printStackTrace(System.err);

            return -1;
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
            	System.err.println("ERROR: executeUpdate() silent - SQLException ");
		e.printStackTrace(System.err);
            }
        }
        return index;
    }


  public static ArrayList<HashMap<String,String>> getResultSet(DataSource ds,String sql, Object... queryArgs)
    {
        RowMapper<HashMap<String,String>> rowmapper = new RowMapper<HashMap<String, String>>() {
         @Override
         public HashMap<String, String> mapRow(ResultSet rs, int row) throws SQLException,UnsupportedEncodingException
         {
                ResultSetMetaData rsmetada = rs.getMetaData();
                HashMap<String, String> hm = new HashMap<String, String>();
                    for (int i = 1; i <= rsmetada.getColumnCount(); i++) {
                        String columnname = rsmetada.getColumnLabel(i);
                        
                        if ("DATETIME".equals(rsmetada.getColumnTypeName(i))) 
			{
                            if (rs.getString(i) != null) 
			    {
                                hm.put(columnname, rs.getDate(i) + " " + rs.getTime(i));
                            } else 
			    {
                                hm.put(columnname, null);
                            }
                        } 
			else if ("LONGVARCHAR".equals(rsmetada.getColumnTypeName(i)) || 
			         "CHAR".equals(rsmetada.getColumnTypeName(i)) || 
				 "VARCHAR".equals(rsmetada.getColumnTypeName(i))) 
                        {
			    if ( rs.getString(i) != null ) 
                            hm.put(columnname, new String(rs.getString(i).getBytes("UTF-8")));
			    else 
                            hm.put(columnname, null);
                        } 
			else 
                        {
			    if ( rs.getString(i) != null ) 
                            hm.put(columnname, rs.getString(i));
			    else 
                            hm.put(columnname, null);
                        }
                    }
                return hm;
        }
        };
        return getResultSetGeneric(ds,rowmapper,sql,queryArgs);
    }

   
}
