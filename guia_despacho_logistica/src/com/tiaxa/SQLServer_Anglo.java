package com.tiaxa;

import static com.tiaxa.Main.fecha_log;
import java.util.ArrayList;
import java.util.HashMap;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLServer_Anglo {

    public static ArrayList<HashMap<String, String>> getData(String url_coneccion, Integer numero_bascula, Integer max_id) {

        String connectionUrl = url_coneccion;
        ArrayList<HashMap<String, String>> data = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
           
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            String SQL="SELECT t.ProNom as 'PRODUCTO',t.CliSucNom AS 'SUCURSAL_DESTINO',t.CliSucDir AS 'SUCURSAL_DIRECCION',t.CliSucCiuNom AS 'DESTINO', "
                    +" t.PDcDocNro as 'GUIA',t.PesComFec AS 'FECHA_HORA',t.ChoNom AS 'CHOFER',t.ChoDId AS 'RUT_CHOFER', "
                    +" t.ChoDV AS 'DIGITO_RUT',t.VhcPat AS 'PATENTE_CAMION',t.PesAcoPat AS 'PATENTE_ACOPLADO',t.PesTipAcoCod AS 'TIPO_ACOPLADO' "
                    +" ,t.TraNom AS 'TRANSPORTISTA',t.PesNro AS 'N_PESAJE',t.PesVhcComBru AS 'BRUTO',t.PesTotTar AS 'TARA',t.PesVhcComNet AS 'NETO', "
                    +" t.PesBseCod AS 'NUMERO_BASCULA',t.PDcEst AS 'ESTADO_GUIA',max(t.CLOTE) AS 'LOTE',max(t.Glosa1) AS 'SELLO', " 
                    +" max(t.Glosa2) AS 'LEY_REF' ,max(t.Glosa3) AS 'HUM_REF',max(t.Glosa4) AS 'BASCULA',max(t.Glosa5) AS 'NUMERO_CARGA' "
                    +" FROM " 
                    +" (SELECT PRODUCTO.ProNom,CLIENTESUCURSAL.CliSucNom,CLIENTESUCURSAL.CliSucDir,CLIENTESUCURSAL.CliSucCiuNom,PESAJEDOCUMENTO.PDcDocNro,PESAJE.PesComFec,CHOFER.ChoNom ,CHOFER.ChoDId ,CHOFER.ChoDV ,PESAJE.VhcPat,PESAJE.PesAcoPat,PESAJE.PesTipAcoCod, "
                    +" TRANSPORTISTA.TraNom,PESAJEDOCUMENTOCAMPOS.PesNro,PESAJE.PesVhcComBru,PESAJE.PesTotTar,PESAJE.PesVhcComNet,PESAJEDOCUMENTOCAMPOS.PesBseCod,PESAJEDOCUMENTO.PDcEst, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'CLOTE' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS CLOTE, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA1' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa1, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA2' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa2, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA3' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa3, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA4' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa4, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA5' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa5 "
                    +" FROM EtruckDTE.dbo.PESAJEDOCUMENTOCAMPOS PESAJEDOCUMENTOCAMPOS "
                    +" JOIN EtruckDTE.dbo.PESAJE PESAJE on PESAJE.PesNro=PESAJEDOCUMENTOCAMPOS.PesNro "
                    +" join EtruckDTE.dbo.CHOFER CHOFER on CHOFER.ChoId = PESAJE.PesTarChoID "
                    +" join EtruckDTE.dbo.PESAJEDOCUMENTO PESAJEDOCUMENTO on PESAJEDOCUMENTO.PesNro=PESAJE.PesNro "
                    +" join EtruckDTE.dbo.TRANSPORTISTA TRANSPORTISTA on PESAJE.PesTraID = TRANSPORTISTA.TraID "
                    +" join EtruckDTE.dbo.PESAJEDOCUMENTODETALLE PESAJEDOCUMENTODETALLE on  PESAJEDOCUMENTODETALLE.PesNro = PESAJE.PesNro "
                    +" join EtruckDTE.dbo.PRODUCTO PRODUCTO on PRODUCTO.ProCod = PESAJEDOCUMENTODETALLE.PDcProCod "
                    +" join EtruckDTE.dbo.CLIENTESUCURSAL CLIENTESUCURSAL on PESAJEDOCUMENTO.PDcDstCliID = CLIENTESUCURSAL.CliID "
                    +" and CLIENTESUCURSAL.CliSucCod = PESAJEDOCUMENTO.PDcDstCliSucCod "
                    +" where PESAJEDOCUMENTOCAMPOS.PesNro > "+ max_id +" and PESAJEDOCUMENTOCAMPOS.PesBseCod ="+ numero_bascula +" "
                    +" and PRODUCTO.ProNom like '%CONCENTRADO%' ) as t "
                    +" group by t.ChoNom,t.ChoDId,t.ChoDV,t.PesNro,t.PesBseCod,t.PesAcoPat,t.VhcPat,t.PDcDocNro,t.PesTipAcoCod "
                    +" ,t.PesComFec,t.PDcEst,t.PesVhcComBru,t.PesTotTar,t.PesVhcComNet,t.TraNom,t.CliSucNom,t.CliSucDir,t.CliSucCiuNom "
                    +" ,t.ProNom;";     
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL);
            System.out.println("[" + fecha_log() + "]===INICIO RESCATE DATOS SQL SERVER POR MAX NUMERO PESAJE=====");
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                row.put("producto", rs.getString(1).trim());
                row.put("sucursal_destino", rs.getString(2).trim());
                row.put("sucursal_direccion", rs.getString(3).trim());
                row.put("destino", rs.getString(4).trim());
                row.put("guia", rs.getString(5).trim());
                row.put("fecha_hora", rs.getString(6).trim());
                row.put("chofer", rs.getString(7).trim());
                row.put("rut_chofer", rs.getString(8).trim()+"-"+rs.getString(9).trim());
                row.put("patente_camion", rs.getString(10).trim());
                row.put("patente_acoplado", rs.getString(11).trim());
                row.put("tipo_acoplado", rs.getString(12).trim());
                row.put("transportista", rs.getString(13).trim());
                row.put("numero_pesaje", rs.getString(14).trim());
                row.put("bruto", rs.getString(15).trim());
                row.put("tara", rs.getString(16).trim());
                row.put("neto", rs.getString(17).trim());
                row.put("numero_bascula", rs.getString(18).trim());
                row.put("estado_guia", rs.getString(19).trim());
                row.put("lote", rs.getString(20).trim());
                row.put("sello", rs.getString(21).trim());
                row.put("ley_ref", rs.getString(22).trim());
                row.put("humedad_ref", rs.getString(23).trim());
                row.put("bascula", rs.getString(24).trim());
                row.put("numero_carga", rs.getString(25).trim());
                
                data.add(row);
            }
            System.out.println("[" + fecha_log() + "]===TERMINO RESCATE DATOS SQL SERVER POR MAX NUMERO PESAJE======");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
        return data;

    }
    
    public static ArrayList<HashMap<String, String>> getDataUpdate(String url_coneccion, Integer numero_bascula) {

        String connectionUrl = url_coneccion;
        ArrayList<HashMap<String, String>> data = new ArrayList<>();

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            String SQL="SELECT t.ProNom as 'PRODUCTO',t.CliSucNom AS 'SUCURSAL_DESTINO',t.CliSucDir AS 'SUCURSAL_DIRECCION',t.CliSucCiuNom AS 'DESTINO', "
                    +" t.PDcDocNro as 'GUIA',t.PesComFec AS 'FECHA_HORA',t.ChoNom AS 'CHOFER',t.ChoDId AS 'RUT_CHOFER', "
                    +" t.ChoDV AS 'DIGITO_RUT',t.VhcPat AS 'PATENTE_CAMION',t.PesAcoPat AS 'PATENTE_ACOPLADO',t.PesTipAcoCod AS 'TIPO_ACOPLADO' "
                    +" ,t.TraNom AS 'TRANSPORTISTA',t.PesNro AS 'N_PESAJE',t.PesVhcComBru AS 'BRUTO',t.PesTotTar AS 'TARA',t.PesVhcComNet AS 'NETO', "
                    +" t.PesBseCod AS 'NUMERO_BASCULA',t.PDcEst AS 'ESTADO_GUIA',max(t.CLOTE) AS 'LOTE',max(t.Glosa1) AS 'SELLO', " 
                    +" max(t.Glosa2) AS 'LEY_REF' ,max(t.Glosa3) AS 'HUM_REF',max(t.Glosa4) AS 'BASCULA',max(t.Glosa5) AS 'NUMERO_CARGA' "
                    +" FROM " 
                    +" (SELECT PRODUCTO.ProNom,CLIENTESUCURSAL.CliSucNom,CLIENTESUCURSAL.CliSucDir,CLIENTESUCURSAL.CliSucCiuNom,PESAJEDOCUMENTO.PDcDocNro,PESAJE.PesComFec,CHOFER.ChoNom ,CHOFER.ChoDId ,CHOFER.ChoDV ,PESAJE.VhcPat,PESAJE.PesAcoPat,PESAJE.PesTipAcoCod, "
                    +" TRANSPORTISTA.TraNom,PESAJEDOCUMENTOCAMPOS.PesNro,PESAJE.PesVhcComBru,PESAJE.PesTotTar,PESAJE.PesVhcComNet,PESAJEDOCUMENTOCAMPOS.PesBseCod,PESAJEDOCUMENTO.PDcEst, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'CLOTE' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS CLOTE, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA1' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa1, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA2' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa2, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA3' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa3, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA4' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa4, "
                    +" (CASE WHEN ltrim(RTRIM(PESAJEDOCUMENTOCAMPOS.PDcCamCod)) = 'GLOSA5' THEN PESAJEDOCUMENTOCAMPOS.PDcCamVal END) AS Glosa5 "
                    +" FROM EtruckDTE.dbo.PESAJEDOCUMENTOCAMPOS PESAJEDOCUMENTOCAMPOS "
                    +" JOIN EtruckDTE.dbo.PESAJE PESAJE on PESAJE.PesNro=PESAJEDOCUMENTOCAMPOS.PesNro "
                    +" join EtruckDTE.dbo.CHOFER CHOFER on CHOFER.ChoId = PESAJE.PesTarChoID "
                    +" join EtruckDTE.dbo.PESAJEDOCUMENTO PESAJEDOCUMENTO on PESAJEDOCUMENTO.PesNro=PESAJE.PesNro "
                    +" join EtruckDTE.dbo.TRANSPORTISTA TRANSPORTISTA on PESAJE.PesTraID = TRANSPORTISTA.TraID "
                    +" join EtruckDTE.dbo.PESAJEDOCUMENTODETALLE PESAJEDOCUMENTODETALLE on  PESAJEDOCUMENTODETALLE.PesNro = PESAJE.PesNro "
                    +" join EtruckDTE.dbo.PRODUCTO PRODUCTO on PRODUCTO.ProCod = PESAJEDOCUMENTODETALLE.PDcProCod "
                    +" join EtruckDTE.dbo.CLIENTESUCURSAL CLIENTESUCURSAL on PESAJEDOCUMENTO.PDcDstCliID = CLIENTESUCURSAL.CliID "
                    +" and CLIENTESUCURSAL.CliSucCod = PESAJEDOCUMENTO.PDcDstCliSucCod "
                    +" where CAST(PESAJE.PesComFec AS date)>=CAST(DATEADD(day, -1, GETDATE()) AS date) and CAST(PESAJE.PesComFec AS date) <= CAST(DATEADD(day, +1, GETDATE()) AS date) "
                    +" and PESAJEDOCUMENTOCAMPOS.PesBseCod ="+ numero_bascula +" "
                    +" and PRODUCTO.ProNom like '%CONCENTRADO%' ) as t "
                    +" group by t.ChoNom,t.ChoDId,t.ChoDV,t.PesNro,t.PesBseCod,t.PesAcoPat,t.VhcPat,t.PDcDocNro,t.PesTipAcoCod "
                    +" ,t.PesComFec,t.PDcEst,t.PesVhcComBru,t.PesTotTar,t.PesVhcComNet,t.TraNom,t.CliSucNom,t.CliSucDir,t.CliSucCiuNom "
                    +" ,t.ProNom;"; 
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL);

            System.out.println("[" + fecha_log() + "]==INICIO RESCATE DATOS SQL SERVER PARA ACTUALIZAR REGISTROS DE AYER Y HOY===");
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                row.put("producto", rs.getString(1).trim());
                row.put("sucursal_destino", rs.getString(2).trim());
                row.put("sucursal_direccion", rs.getString(3).trim());
                row.put("destino", rs.getString(4).trim());
                row.put("guia", rs.getString(5).trim());
                row.put("fecha_hora", rs.getString(6).trim());
                row.put("chofer", rs.getString(7).trim());
                row.put("rut_chofer", rs.getString(8).trim()+"-"+rs.getString(9).trim());
                row.put("patente_camion", rs.getString(10).trim());
                row.put("patente_acoplado", rs.getString(11).trim());
                row.put("tipo_acoplado", rs.getString(12).trim());
                row.put("transportista", rs.getString(13).trim());
                row.put("numero_pesaje", rs.getString(14).trim());
                row.put("bruto", rs.getString(15).trim());
                row.put("tara", rs.getString(16).trim());
                row.put("neto", rs.getString(17).trim());
                row.put("numero_bascula", rs.getString(18).trim());
                row.put("estado_guia", rs.getString(19).trim());
                row.put("lote", rs.getString(20).trim());
                row.put("sello", rs.getString(21).trim());
                row.put("ley_ref", rs.getString(22).trim());
                row.put("humedad_ref", rs.getString(23).trim());
                row.put("bascula", rs.getString(24).trim());
                row.put("numero_carga", rs.getString(25).trim());
                data.add(row);
            }
            System.out.println("[" + fecha_log() + "]===TERMINO RESCATE DATOS SQL SERVER PARA ACTUALIZAR REGISTROS DE AYER Y HOY====");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
        return data;

    }

}
