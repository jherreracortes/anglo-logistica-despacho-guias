package com.tiaxa;

import static com.tiaxa.SQLServer_Anglo.getData;
import static com.tiaxa.SQLServer_Anglo.getDataUpdate;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.ResultSet;
import org.postgresql.ds.PGPoolingDataSource;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Main {

    static final PGPoolingDataSource pool = new PGPoolingDataSource();
    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());

    public static void main(String[] args)
            throws SQLException, ParseException, Exception {

        String url = System.getProperty("CFG_FILE");
        Properties prop = getPlatformConfig(new File(url));
        setupDataBase(prop);
        ArrayList<Basculas> basculas_config = getBasculasConfig();

        for (Basculas bascula : basculas_config) {
            runSummary(bascula);
        }

    }

    public static boolean isValidDate(String proc_date) {
        try {

            sdf.setLenient(false);
            sdf.parse(proc_date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<Basculas> getBasculasConfig() {

        String sql = "select numero_bascula, url_coneccion, max_id,descripcion,origen,id  from config.config_guia_despacho order by id asc ;";

        RowMapper<Basculas> mapper = (ResultSet rs, int row) -> {
            Basculas bas = new Basculas();
            bas.numero_bascula = rs.getInt(1);
            bas.url_coneccion = rs.getString(2);
            bas.max_id = rs.getInt(3);
            bas.descripcion = rs.getString(4);
            bas.origen = rs.getString(5);
            bas.id = rs.getInt(6);

            return bas;
        };

        return JDBCUtilities.getResultSetGeneric(pool, mapper, sql);
    }

    public static void runSummary(Basculas bas) throws Exception {
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] Numero De Bascula    : " + bas.numero_bascula);
        System.out.println("[" + fecha_log() + "] URL Coneccion   : " + bas.url_coneccion);
        System.out.println("[" + fecha_log() + "] MAX ID  : " + bas.max_id);
        System.out.println("[" + fecha_log() + "] ===============================");
        ArrayList<HashMap<String, String>> data_guia_despacho;
        ArrayList<HashMap<String, String>> data_guia_despacho_update;
        data_guia_despacho = getData(bas.url_coneccion, bas.numero_bascula, bas.max_id);
        data_guia_despacho_update=getDataUpdate(bas.url_coneccion, bas.numero_bascula);
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] ========GUARDANDO REGISTROS EN POSTGRESQL=============");
        guardaBasculas(pool, data_guia_despacho, bas.origen);
        guardaBasculas(pool, data_guia_despacho_update, bas.origen);
        System.out.println("[" + fecha_log() + "] ========TERMINO DE GUARDADO DE REGISTROS EN POSTGRESQL=============");
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] ========ACTUALIZACION DE MAX ID==========");
        updateMaxId(pool, bas.numero_bascula, bas.id, bas.origen);
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] DATA RESCATADA DE SQL SERVER ANGLO");

        data_guia_despacho.forEach((jj) -> {
            System.err.println(jj);
        });

        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] DATA RESCATADA DE SQL SERVER ANGLO DIA ANTERIOR + DIA ACTUAL");
        data_guia_despacho_update.forEach((jj) -> {
            System.err.println(jj);
        });
        System.out.println("[" + fecha_log() + "]===============================");

    }

    public static void setupDataBase(Properties props) {
        pool.setApplicationName(props.getProperty("TAG", "NO-NAME"));
        pool.setServerName(props.getProperty("DB_HOST"));
        pool.setDatabaseName(props.getProperty("DB_NAME"));
        pool.setUser(props.getProperty("DB_USER"));
        pool.setPortNumber(Integer.parseInt(props.getProperty("DB_PORT")));
        pool.setPassword(props.getProperty("DB_PASSWORD"));
        pool.setMaxConnections(1);
    }

    public static Properties getPlatformConfig(File confFile) {

        Properties properties = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(confFile);
            properties.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return properties;
    }

    public static void updateMaxId(PGPoolingDataSource pool, Integer numero_bascula, Integer id, String origen) {
        Connection c = null;
        PreparedStatement stmt = null;

        try {
            c = pool.getConnection();
            c.setAutoCommit(false);
            String sql = "UPDATE config.config_guia_despacho SET max_id=(select COALESCE(max(numero_pesaje),0) from work.guia_despacho where bascula=?) where numero_bascula=? and id=?;";

            stmt = c.prepareStatement(sql);

            stmt.setInt(1, numero_bascula);
            stmt.setInt(2, numero_bascula);
            stmt.setInt(3, id);
            stmt.executeUpdate();

            c.commit();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(-1);
        }

    }

    public static void guardaBasculas(PGPoolingDataSource pool, ArrayList<HashMap<String, String>> data_guia, String origen) {
        Connection c = null;
        PreparedStatement stmt = null;
       
        try {
            c = pool.getConnection();
            c.setAutoCommit(false);
            String sql = "select work.insert_guia_despacho(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            stmt = c.prepareStatement(sql);
            for (HashMap<String, String> data : data_guia) {
                stmt.setString(1, data.get("producto"));
                stmt.setString(2, data.get("sucursal_destino"));
                stmt.setString(3, data.get("sucursal_direccion"));
                stmt.setString(4, data.get("destino"));
                stmt.setInt(5, Integer.parseInt(data.get("guia")));
                stmt.setString(6, data.get("fecha_hora"));
                stmt.setString(7, data.get("chofer"));
                stmt.setString(8, data.get("rut_chofer"));
                stmt.setString(9, data.get("patente_camion"));
                stmt.setString(10, data.get("patente_acoplado"));
                stmt.setString(11, data.get("tipo_acoplado"));
                stmt.setString(12, data.get("transportista"));
                stmt.setInt(13, Integer.parseInt(data.get("numero_pesaje")));
                stmt.setInt(14, Integer.parseInt(data.get("bruto")));
                stmt.setInt(15, Integer.parseInt(data.get("tara")));
                stmt.setInt(16, Integer.parseInt(data.get("neto")));
                stmt.setInt(17, Integer.parseInt(data.get("numero_bascula")));
                stmt.setString(18, data.get("estado_guia"));
                stmt.setString(19, data.get("lote"));
                stmt.setString(20, data.get("sello"));
                stmt.setString(21, data.get("ley_ref"));
                stmt.setString(22, data.get("humedad_ref"));
                stmt.setString(23, data.get("bascula"));
                stmt.setString(24, data.get("numero_carga"));
                stmt.execute();
            }
            c.commit();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(-1);
        }

    }
    
    public static String fecha_log() {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
                formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdfl = new SimpleDateFormat(formatFecha);
		finalDate = sdfl.format(calendario.getTime());

		return finalDate;
	}

}
