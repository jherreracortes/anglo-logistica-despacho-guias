DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${DIR}
#echo `pwd`
date=$1
if [ -z "${date}" ]
  then
    date=$(date +"%Y-%m-%d" --date='1 day ago')
fi
log_file=$(date +"%Y%m%d%H")
CFG_FILE=./cfg/guia_despacho.cfg
#echo ${DB_URL}
#echo ${date}
java -jar -DCFG_FILE="${CFG_FILE}"  ./guia_despacho_logistica.jar ${date} >> ./log/guia_despacho_logistica.${log_file}.log  2>&1 
